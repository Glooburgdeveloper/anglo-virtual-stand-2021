<?php header("Access-Control-Allow-Origin: *");?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Mining Indaba Virtual Exhibition Stand</title>
        <meta name="description" content="Even though the world has changed, our purpose stays the same: to re-imagine mining to improve people’s lives." />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />

        <!-- for Google -->
        <meta name="language" content="English">
        <meta name="robots" content="index, follow">
        <meta name="description" content="Even though the world has changed, our purpose stays the same: to re-imagine mining to improve people’s lives."/>
        <meta name="author" content="Ogilvy Lab South Africa" />
        <meta name="copyright" content="Copyright ©Anglo American. All Rights Reserved." />
        <meta name="application-name" content="Mining Indaba Virtual Exhibition Stand" />
        <meta name="theme-color" content="#002776">
        <meta name="apple-mobile-web-app-status-bar-style" content="red">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">

        <link rel="icon" type="image/png" href="img/favicon.png" sizes="16x16">
        <link rel="icon" type="image/png" href="img/favicon.png" sizes="32x32">
        <link rel="icon" type="image/png" href="img/favicon.png" sizes="96x96">

        <meta property="og:site_name" content="Mining Indaba Virtual Exhibition Stand">
        <meta name="twitter:image:alt" content="Share....">

        <!-- Twitter Card -->
        <meta name="twitter:site" content="@AngloAmerican">
        <meta name="twitter:title" content="Mining Indaba Virtual Exhibition Stand">
        <meta name="twitter:description" content="Even though the world has changed, our purpose stays the same: to re-imagine mining to improve people’s lives.">
        <meta name="twitter:image" content="https://virtualstand.angloamerican.com/img/share-image.jpg">
        <meta name="twitter:card" content="summary_large_image">

        <!-- for Facebook -->
        <meta property="fb:app_id" content="2926298907642803" />
        <meta property="og:title" content="Mining Indaba Virtual Exhibition Stand"  />
        <meta property="og:image" content="https://virtualstand.angloamerican.com/img/share-image.jpg" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="https://virtualstand.angloamerican.com/" />
        <meta property="og:description" content="Even though the world has changed, our purpose stays the same: to re-imagine mining to improve people’s lives." />

        <!-- Google Tag Manager Anglo -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5KHTC9K');</script>
        <!-- End Google Tag Manager -->

        <link rel="prefetch" as="image" href="img/front-view.jpg">

        <link rel="stylesheet" href="css/swiper-bundle.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body class="multiple-scenes view-control-buttons" id="HomePage">
    <!-- Google Tag Manager (noscript) Anglo -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5KHTC9K"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="pano"></div>

        <div id="titleBar" class="border-anglo">
            <h1 class="sceneName"></h1>
        </div>

        <div id="topNav">
            <div id="sceneList">
                <ul class="scenes">
                    <li class="text welcome-desk"><a href="javascript:void(0)" class="scene" data-id="welcome-desk">Welcome Desk</a></li>
                    <li class="text speaker-profiles sub">
                        Speaker Information
                        <ul>
                            <li><a href="javascript:void(0)" class="scene" id="exploreThis" data-id="speaker-profiles">Our speakers</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="5-left-wall-desk">Safety first</a></li>
                        </ul>
                    </li>
                    <li class="text community-wall sub">
                        Community Wall
                        <ul>
                            <li><a href="javascript:void(0)" class="scene" data-id="community-wall">Our Social Way</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="4-right-wall-desk">Sustainable Mining Plan</a></li>
                        </ul>
                    </li>
                    <li class="text motivation-board sub">
                       Our Living Purpose Wall
                        <ul>
                            <li><a href="javascript:void(0)" class="scene" data-id="6-left-wall">Our Purpose</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="6-left-wall">Our WeCare Programme</a></li>
                        </ul>
                    </li>
                    <li class="text sub long hot-desk"">
                        Innovation Hub
                        <ul>
                            <li><a href="javascript:void(0)" class="scene" data-id="3-center-view">Brochure downloads</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="3-center-view-connect">Connect with us</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="innobox-panels-1">Futuresmart Mining<sup>TM</sup></a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="innobox-panels-2">Step-change innovations</a></li>
                        </ul>
                    </li>
                    <li class="text sub worldwide-display">
                        Where we operate
                        <ul>
                            <li><a href="javascript:void(0)" class="scene" data-id="3-center-view-speak">Come chat to us</a></li>
                            <li><a href="javascript:void(0)" class="scene" data-id="3-center-view">Our global footprint</a></li>
                        </ul>
                    </li>
                    <li class="text discussion-wall"><a href="javascript:void(0)" class="scene" data-id="discussion-wall">Thank You</a></li>
                </ul>
            </div>

            <a id="sceneListToggle" href="javascript:void(0)" >
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
            </a>
        </div>

        <div id="viewControls">
            <div class="control-part direction-controls">
                <a href="javascript:void(0)" id="viewUp" class="viewControlButton viewControlButton-1">
                    <img class="icon" src="img/icons/icon-arrow-up-blue.svg">
                </a>
                <a href="javascript:void(0)" id="viewDown" class="viewControlButton viewControlButton-2">
                    <img class="icon" src="img/icons/icon-arrow-down-blue.svg">
                </a>
                <a href="javascript:void(0)" id="viewLeft" class="viewControlButton viewControlButton-3">
                    <img class="icon" src="img/icons/icon-arrow-left-blue.svg">
                </a>
                <a href="javascript:void(0)" id="viewRight" class="viewControlButton viewControlButton-4">
                    <img class="icon" src="img/icons/icon-arrow-right-blue.svg">
                </a>
            </div>

            <div class="control-part zoom-controls">
                <a href="javascript:void(0)" id="viewIn" class="viewControlButton viewControlButton-5">
                    <img class="icon" src="img/icons/icon-zoom-in.svg">
                </a>
                <a href="javascript:void(0)" id="viewOut" class="viewControlButton viewControlButton-6">
                    <img class="icon" src="img/icons/icon-zoom-out.svg">
                </a>
            </div>

            <div class="control-part">
                <a href="javascript:void(0)" id="autorotateToggle" class="viewControlButton">
                    <img class="icon off" src="img/icons/icon-pan-active.svg">
                    <img class="icon on" src="img/icons/icon-pan-inactive.svg">
                </a>
                <a href="javascript:void(0)" id="fullscreenToggle" class="viewControlButton">
                    <img class="icon off" src="img/icons/icon-fullscreen-expand.svg">
                    <img class="icon on" src="img/icons/icon-fullscreen-minimise.svg">
                </a>
                <div id="toggleDeviceOrientation" class="viewControlButton">
                    <img class="icon off" src="img/icons/icon-gyroscope-active.svg">
                    <img class="icon on" src="img/icons/icon-gyroscope-inactive.svg">
                </div>
            </div>
        </div>

        <div class="page-loader" id="page-loader">
            <div class="loader-bar"></div>
        </div>

        <div id="rotateScreen">
            <span class="icon"></span>
            <h1>Our website is best<br/> viewed in portrait</h1>
            <h2>Please rotate your display</h2>
        </div>

        <div id="modalHolder"></div>

        <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
        <script src="js/screenfull.js" ></script>
        <script src="js/marzipano.js" ></script>
        <script src="js/DeviceOrientationControlMethod.js"></script>
        <script src="js/swiper-bundle.min.js"></script>
        <script src="js/data.js"></script>
        <script src="js/index.js"></script>

    </body>
</html>
