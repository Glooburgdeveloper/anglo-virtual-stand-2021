
<div id="mainModal" class="anglo-modal connect">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Connect with us on social media
                </h1>
                <p>
                    Follow us on social media to stay up to date with our latest news and developments. If you have any other queries, please chat to one of our team members on the stand.
                    <br/> <br/>
                    <a data-modal="speak-to-us" href="javascript:void(0)" class="cta-link" id="chatBtn">Chat to us</a>
                </p>
            </div>
            <div class="info-base">
                <a class="social linkedin" target="_blank" href="https://www.linkedin.com/company/anglo-american"></a>
                <a class="social facebook" target="_blank" href="https://www.facebook.com/angloamerican"></a>
                <a class="social twitter" target="_blank" href="https://twitter.com/AngloAmerican"></a>
                <a class="social youtube" target="_blank" href="https://www.youtube.com/user/angloamerican"></a>
<!--                <button class="anglo-btn" id="contactUs" data-link="https://southafrica.angloamerican.com/site-services/contact-us ">Contact us</button>-->
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>
<script>
    // button control
    // $('.anglo-btn').off().on('click', function(){
    //     let linkUrl = $('.anglo-btn').attr('data-link');
    //     window.open(linkUrl, '_blank');
    // });
</script>