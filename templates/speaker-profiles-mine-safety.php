
<div id="mainModal" class="anglo-modal mine-safety">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Safety is always first on our agenda
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            We truly believe that all injuries are preventable and that by working together we can make
                            safety a way of life, inside and outside the workplace.
                        </p>
                        <p>
                            As part of our ongoing response to Covid-19, we have introduced 9 workplace
                            controls to keep our employees safe, so that they can continue to provide for their families and communities.
                        </p>
                        <ol>
                            <li>Self-monitoring and reporting</li>
                            <li>Education and communication</li>
                            <li>Screening and testing</li>
                            <li>Sanitary cordon</li>
                            <li>Social distancing</li>
                            <li>Hygiene and cleaning measures</li>
                            <li>De-densification of workplaces</li>
                            <li>Respiratory protection</li>
                            <li>Covid-19 contact tracing</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/udBYd2wIaqQ?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>