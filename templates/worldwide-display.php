
<div id="mainModal" class="anglo-modal worldwide-display">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Where we operate
                </h1>
                <p>
                    Explore our global footprint and how we are <b>#MiningWithPurpose</b> across the world.
                </p>
            </div>
            <div class="media-panel">
                <img src="img/worldwide-map.jpg" alt="">
                <button class="anglo-btn" data-link="https://www.angloamerican.com/about-us/where-we-operate">View interactive map</button>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>

<script>
    // button control
    $('.anglo-btn').off().on('click', function(){
        let linkUrl = $('.anglo-btn').attr('data-link');
        window.open(linkUrl, '_blank');
    })
</script>