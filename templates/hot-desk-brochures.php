
<div id="mainModal" class="anglo-modal brochures">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Brochures
                </h1>
                <p>
                    Learn more about Anglo American and see how we’re <b>#MiningWithPurpose</b>.
                </p>
            </div>
            <div class="info-base">
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <a class="media-link" target="_blank" href="downloads/aa-corporate-factsheet-2020.pdf" download>Quick facts</a>
                        <a class="media-link" target="_blank" href="downloads/aa-annual-report-2019.pdf" download>Annual report</a>
                        <a class="media-link" target="_blank" href="downloads/aa-sustainability-report-2019.pdf" download>Sustainability report</a>
                        <a class="media-link" target="_blank" href="downloads/aa-transformation-report-2019.pdf" download>Transformation report</a>
                        <a class="media-link" target="_blank" href="downloads/aa-sustainable-mining-plan.pdf" download>Sustainable Mining plan</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>