
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Bulk sorting
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        We are exploring pre-concentration techniques such as screening and sorting that use the natural variability
                        of the ore body. The process is simpler, less expensive and has a much smaller footprint than the current
                        individual particle sorting technologies available.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/Ml8jOnJ0bX0?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>