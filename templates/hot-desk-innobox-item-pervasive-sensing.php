
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Pervasive sensing
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Conventional flow sensors require electricity and dedicated communications networks.
                    </p>
                    <p>
                        Did you know that optical fibre can be used as a listening device? Imagine using optical fibre
                        instead of thousands of microphones, with every metre of fibre representing another microphone.
                        By allowing us to better monitor and understand the flow around the plant, it makes it possible
                        to meter flow within pipes: delivering a low-cost, non-intrusive solution that can measure
                        several flows simultaneously.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/Mgkyto1uo8U?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>