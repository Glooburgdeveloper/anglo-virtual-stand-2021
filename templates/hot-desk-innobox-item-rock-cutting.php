
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Rock cutting
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Underground hard rock mining continues to pose a risk to the working environment, while the difficulties
                        of hard rock cutting remains a key obstacle to overcome. Anglo American is attempting to deliver low-profile
                        hard rock cutting that can exploit valuable narrow PGM reefs without excessive dilution or large tunnel boring machines.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/NzpRtTkUlaA?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>