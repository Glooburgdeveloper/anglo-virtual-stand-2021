
<div id="mainModal" class="anglo-modal innobox">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Step-change <br/>innovations
                </h1>
                <div class="swiper-pagination"></div>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            Explore some of the step-change innovations that will transform the nature of mining – how we source,
                            mine, process, move and market our products – and how our stakeholders experience our business. It is
                            about transforming our physical and societal footprint.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <!-- Slider main container -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-ore-control" class="profile">
                                    <img src="img/innobox-vboc.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Value based ore control
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-ore-control" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-course-particle-recovery" class="profile">
                                    <img src="img/innobox-coarse-particle-recovery.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Course particle recovery
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-course-particle-recovery" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-swarm-robotics" class="profile">
                                    <img src="img/innobox-swarm-robotics.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Swarm robotics
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-swarm-robotics" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-bulk-sorting" class="profile">
                                    <img src="img/innobox-bulk-sorting.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Bulk sorting
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-bulk-sorting" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-rock-cutting" class="profile">
                                    <img src="img/innobox-rock-cutting.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Rock cutting
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-rock-cutting" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-shockbreak" class="profile">
                                    <img src="img/innobox-shockbreak.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Shockbreak
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-shockbreak" class="anglo-btn">Learn more</button>
                        </div>

                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item-pervasive-sensing" class="profile">
                                    <img src="img/innobox-pervasice-sensing.png" alt="">
                                </div>
                                <div class="bio">
                                    <h1>
                                        Pervasive sensing
                                    </h1>
                                </div>
                            </div>
                            <button data-modal="hot-desk-innobox-item-pervasive-sensing" class="anglo-btn">Learn more</button>
                        </div>


                    </div>
                </div>
                <div class="swiper-shadow-box"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>

    </div>
</div>
<div id="secModal" class="anglo-modal">
</div>
<div id="mainOverlay" class="overlay"></div>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        spaceBetween: 0,
        loop: true,


        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    // secondary modal control
    $('.profile, .anglo-btn').off().on('click', function(){
        let activeSec = $(this).attr('data-modal');
        $('#mainModal').removeClass('active');
        $.get("templates/" + activeSec + ".php", function (data) {
            $('#secModal').append(data);
            setTimeout(function(){
                var element = document.getElementById('secModal');
                element.classList.add('active');
            }, 100);

            $('#close-sec-modal-btn').off().on('click', function(){
                $('#secModal').removeClass('active');
                setTimeout(function(){
                    $('#secModal .modal-info').remove();
                    $('#mainModal').addClass('active');
                }, 200);
            })
        });
    })
</script>