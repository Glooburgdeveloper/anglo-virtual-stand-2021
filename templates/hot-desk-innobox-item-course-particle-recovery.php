
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Course particle recovery
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Current flotation technology requires large infrastructure and has very high-energy needs. Newly developed
                        HydroFloat technology leverages a fluidised bed and flotation to reduce infrastructure and power required.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/zlw0rIBnYVc?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>