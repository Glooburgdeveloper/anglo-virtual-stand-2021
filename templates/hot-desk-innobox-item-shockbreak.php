
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Shockbreak
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Breaking rocks accounts for over 3 million megawatt-hours of energy consumed each year on our planet.
                        Through new comminution methods, we can deliver impact breakage with reduced energy consumption. Anglo American
                        has committed to the technology and is currently pursuing an accelerated testing programme.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/ut4gI7Kqsjc?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>