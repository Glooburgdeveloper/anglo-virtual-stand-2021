
<div id="mainModal" class="anglo-modal welcome">
    <div class="modal-info welcome">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Welcome to <br/>Anglo American’s<br/> virtual exhibition stand.
                </h1>
                <p>
                    Even though the world has changed, our purpose stays the same: to re-imagine mining to improve people’s lives.
                </p>
                <p>
                    Learn more about our business by exploring our stand and by chatting to our team.
                    <br/> <br/>
                    <a data-modal="speak-to-us" href="javascript:void(0)" class="cta-link" id="chatBtn">Chat to us</a>
                    <a data-modal="welcome-desk-floorplan" href="javascript:void(0)" class="cta-link" id="floorPlan">View floor plan</a>
                </p>
            </div>
            <div class="info-base">
                <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/972617758%3Fsecret_token%3Ds-eXSipU0Ypl8&color=%23002776&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                <button class="anglo-btn" id="eploreStart">Start exploring</button>
            </div>
        </div>

    </div>
</div>
<div id="secModal" class="anglo-modal welcome-floor">
</div>
<div id="mainOverlay" class="overlay"></div>
<script>
    // secondary modal control
    $('#floorPlan').off().on('click', function(){
        let activeSec = $(this).attr('data-modal');
        $('#mainModal').removeClass('active');
        $.get("templates/" + activeSec + ".php", function (data) {
            $('#secModal').append(data);
            setTimeout(function(){
                var element = document.getElementById('secModal');
                element.classList.add('active');
            }, 100);

            $('#close-sec-modal-btn').off().on('click', function(){
                $('#secModal').removeClass('active');
                setTimeout(function(){
                    $('#secModal .modal-info').remove();
                    $('#mainModal').addClass('active');
                }, 200);
            })
        });
    });



</script>