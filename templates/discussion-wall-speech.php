
<div id="mainModal" class="anglo-modal thanks ">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Thank You note from Mark Cutifani.
                </h1>
                <p>
                    Thank you for joining us and exploring how we mine with purpose. We look forward to engaging with you again.
                </p>
                <img src="img/Mark-Cutifani-signature.svg" alt="">
            </div>
            <div class="media-panel">
                <img src="img/mark-cutifani.png" alt="">
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>