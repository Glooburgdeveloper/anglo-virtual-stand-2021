
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                SmartPower
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        What if we could replace the 1,5 billion litres of diesel currently used across our operating sites?
                        Our SmartPower project has the potential to turn this vision into a reality. By creating a retrofitted
                        hybrid hydrogen-electric haul truck that generates hydrogen own-site via electrolysis. It may help us
                        achieve our goal of a net reduction of 30% of greenhouse gas emissions by 2030.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/6TKaHzCT4YY?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>