
<div id="mainModal" class="anglo-modal">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    New World. Same Purpose.
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            Our Sustainable Mining Plan is fostering innovation and will deliver step-change results across the entire mining value chain,
                            from mineral discovery right through to our customers hands.
                        </p>
                        <p>
                            Learn more about our Sustainable Mining Plan and how it’s designed to drive improvements, resilience and agility in our business.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/bUEYKNK6XEM?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>