
<div id="mainModal" class="anglo-modal future-smart">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    FutureSmart Mining<sup>TM</sup>
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                           <b>Technology, digitalisation and sustainability working hand in hand</b>
                        </p>
                        <p>
                            FutureSmart Mining<sup>TM</sup> is our innovation-led pathway to sustainable mining.
                            These are the step-change innovations that will transform the nature of mining – how we source, mine, process, move and market our products –
                            and how our stakeholders experience our business. It is about transforming our physical and societal footprint.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/HOMkaq1_T1I?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>