
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Value based ore control
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Value Based Ore Control (VBOC), allows operations to extract geological information from the ground ahead of time.
                        With VBOC, mining is no longer just about tonnes and grade, it eliminates reactive decision making to reveal important
                        characteristics of the rock before mining commences.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/BwXtrigC4gs?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>