
<div id="mainModal" class="anglo-modal wecare">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Covid-19. Working together to provide the right support at the right time.
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            Through our WeCare Programme, as part of our ongoing response to Covid-19, we continue to work with governments,
                            our employees and host communities, to help identify and address their immediate needs. We committed over R650 million
                            toward food parcels, masks, hand sanitisers, drinking water and medical equipment, alongside screening stations,
                            free testing, hospital beds, isolation facilities and much more.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/0jJryxZSFak?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>