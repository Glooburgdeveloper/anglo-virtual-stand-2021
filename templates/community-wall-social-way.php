
<div id="mainModal" class="anglo-modal social-way">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Social Way, our management system for social performance
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            The Anglo American Social Way is our management system for social performance. It’s how we create a
                            sustainable environment where our stakeholders can prosper. See how we’re implementing our Social Way
                            across our operations.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/wwhTAQFkx_0?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</div>