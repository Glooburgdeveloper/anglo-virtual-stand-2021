
<div id="mainModal" class="anglo-modal speakers">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Our speakers
                </h1>
                <p>
                    <b>Mark Cutifani: </b>Chief Executive, Anglo American<br/>
                    <b>Natascha Viljoen: </b>Chief Executive Officer, Anglo American Platinum</b>
                </p>
                <div class="swiper-pagination"></div>
            </div>
            <div class="media-panel">
                <!-- Slider main container -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item" class="profile">
                                    <h1>
                                        Mark Cutifani
                                        <span>Anglo American</span>
                                        <span>Chief Executive</span>
                                    </h1>
                                    <img src="img/profiles/profile-1.png" alt="">
                                </div>
                                <div class="bio">
                                    <span class="topic">
                                        Speaker Session: Re-igniting Mining Capital: ESG Investing in a Covid-19 recovery world
                                    </span>
                                    <div class="mobi-scroll">
                                        <div class="scroll-content">
                                           <span>
                                                2 February 2021, 13:25 – 14:45 (GMT)
                                            </span>
                                            <p>
                                                Mark Cutifani contributes to Anglo American over 40 years’ experience of the mining industry across a wide range
                                                of geographies and commodities. He is Chair of the Group Management Committee (GMC), is a non-executive director
                                                of Anglo American Platinum, chairman of Anglo American South Africa and chairman of De Beers. Mark was previously
                                                CEO of AngloGold Ashanti Limited, a position he held from 2007-2013. Before joining AngloGold Ashanti, Mark was COO
                                                at Vale Inco, where he was responsible for Vale’s global nickel business. He has also held senior executive positions
                                                with the Normandy Group, Sons of Gwalia, Western Mining Corporation, Kalgoorlie Consolidated Gold Mines and CRA (Rio Tinto.)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="profile-item">
                                <div data-modal="hot-desk-innobox-item" class="profile">
                                    <h1>
                                        Natascha Viljoen
                                        <span>Anglo American Platinum</span>
                                        <span>Chief Executive Officer</span>
                                    </h1>
                                    <img src="img/profiles/profile-2.png" alt="">
                                </div>
                                <div class="bio">
                                    <span class="topic">
                                        Speaker Session: Green Metals, PMGs and Global De-Carbonisation
                                    </span>
                                    <div class="mobi-scroll">
                                        <div class="scroll-content">
                                            <span>
                                                3 February 2021, 16:10 – 17:10 (GMT)
                                            </span>
                                            <p>
                                                Natascha Viljoen was appointed as CEO of Anglo American Platinum in April 2020 and is also a member of the
                                                Anglo American plc group management committee. She joined Anglo American in 2014 as Group Head of Processing,
                                                following six years as an executive vice-president at Lonmin. Her experience in the mining industry covers a
                                                range of commodities, including PGMs, coal, gold, copper, diamonds, phosphates, chrome, iron ore and uranium,
                                                and roles ranging from starting up a new mine to running global processing operations.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-shadow-box"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>
<div id="mainOverlay" class="overlay"></div>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        spaceBetween: 0,
        loop: true,


        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
</script>