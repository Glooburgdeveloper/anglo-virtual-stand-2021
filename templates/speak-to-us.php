
<div id="mainModal" class="anglo-modal speak-to-us">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Chat to us now
                </h1>
                <p>
                    Engage with our team through a live chat and learn more about Anglo American by connecting with one of our representatives.
                </p>
                <div class="swiper-pagination"></div>
                <div class="media-panel">
                    <!-- Slider main container -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Roslyn Jones
                                            <span>Communication Manager Operations</span>
                                            <span>Anglo American Platinum</span>
                                        </h1>
                                        <p>Chat to me about Anglo American Platinum</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 14:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="60127ac8c31c9117cb7357eb/1et42v863">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Dhashinie Naidoo
                                            <span>Event Specialist</span>
                                            <span>Anglo American Platinum</span>
                                        </h1>
                                        <p>Chat to me about Anglo American Platinum</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>14:00 | 16:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="601293f5a9a34e36b9714c7d/1et493u4h">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Boitumelo Tlala
                                            <span>Officer Public Affairs</span>
                                            <span>Kumba Iron Ore</span>
                                        </h1>
                                        <p>Chat to me about Kumba Iron Ore</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="60129505c31c9117cb7361fe/1et49c82q">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Lynn Berowsky
                                            <span>Senior Communications Officer</span>
                                            <span>De Beers Group</span>
                                        </h1>
                                        <p>Chat to me about the De Beers Group</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="601295c0c31c9117cb73623f/1et49huok">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Nkuli Takadimane
                                            <span>Communication Specialist</span>
                                            <span>Coal SA</span>
                                        </h1>
                                        <p>Chat to me about Coal SA</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012965cc31c9117cb736278/1et49mmh1">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Phil Newman
                                            <span>Lead Innovation</span>
                                            <span>FutureSmart Mining<sup>TM</sup></span>
                                        </h1>
                                        <p>Chat to me about FutureSmart Mining<sup>TM</sup></p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="60129712c31c9117cb7362ae/1et49s8er">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            James Johnston
                                            <span>CE Office Principal</span>
                                            <span>FutureSmart Mining<sup>TM</sup></span>
                                        </h1>
                                        <p>Chat to me about FutureSmart Mining<sup>TM</sup></p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>13:00 | 16:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6014188bc31c9117cb73d88a/1et77voqb">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Lesego Mashigo
                                            <span>Communications Manager</span>
                                            <span>Zimele</span>
                                        </h1>
                                        <p>Chat to me about Zimele and small business development</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 16:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012985bc31c9117cb736318/1et4a6a88">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Jacqui Martin
                                            <span>Transformation & Regulatory Specialist</span>
                                            <span>Transformation</span>
                                        </h1>
                                        <p>Chat to me about transformation</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="60129fb1c31c9117cb73655e/1et4bvkc8">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Charlotte Mosiane
                                            <span>Inclusive Procurement Specialist</span>
                                            <span>Supply Chain</span>
                                        </h1>
                                        <p>Chat to me about supply chain</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012a0c0a9a34e36b9715141/1et4c7s2f">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Itumeleng Mogatusi
                                            <span>Sustainability Integration Specialist</span>
                                            <span>Sustainability Strategy</span>
                                        </h1>
                                        <p>Chat to me about sustainable mining </p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012ab4fc31c9117cb7369a5/1et4eqc9k">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Khule Duma
                                            <span>International & Govt Relations Specialist</span>
                                            <span>International & Govt Relations</span>
                                        </h1>
                                        <p>Chat to me about stakeholder relations</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 14:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012abdcc31c9117cb7369e1/1et4eultk">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Maike von Heymann
                                            <span>Head of Collaborative Regional Dev (CRD)</span>
                                            <span>WeCare Programme</span>
                                        </h1>
                                        <p>Chat to me about our WeCare Programme</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 14:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012ac65c31c9117cb736a0a/1et4f2ro0">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Nomonde Ndwalaza
                                            <span>Media Specialist</span>
                                            <span>Corporate Communications</span>
                                        </h1>
                                        <p>Chat to me about media</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012acdbc31c9117cb736a42/1et4f6erb">Chat</button>
                                </div>
                            </div>
                            <!-- Slide -->
                            <div class="swiper-slide">
                                <div class="profile-item">
                                    <div class="profile">
                                        <h1>
                                            Natali Dukhi
                                            <span>Marketing Communication Specialist</span>
                                            <span>Corporate Communications</span>
                                        </h1>
                                        <p>Chat to me about advertising</p>
                                    </div>
                                    <div class="bio">
                                        <div class="info-box">
                                            <span>Time</span>
                                            <p>12:00 | 17:00 (GMT)</p>
                                        </div>
                                        <div class="info-box">
                                            <span>Date</span>
                                            <p>
                                                02/02/2021
                                                03/02/2021
                                            </p>
                                        </div>
                                    </div>
                                    <button class="anglo-btn" data-link="6012add2a9a34e36b97155f0/1et4fdvl9">Chat</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="swiper-shadow-box"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="overlay"></div>
</div>
<div id="secModal" class="anglo-modal">
</div>
<div id="mainOverlay" class="overlay"></div>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        spaceBetween: 0,
        loop: true,


        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    // secondary modal control
    $('.anglo-btn').off().on('click', function(){
        let linkUrl = $(this).attr('data-link');
        let windowLo = "https://tawk.to/chat/" + linkUrl;
        window.open(windowLo, '_blank');
    });
</script>