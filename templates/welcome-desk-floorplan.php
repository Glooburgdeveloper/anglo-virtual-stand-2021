<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <picture>
                <source media="(max-width:500px)" srcset="img/overview-mobile.png">
                <img class="header-deco" src="img/overview-desktop.png" alt="">
            </picture>
        </div>
    </div>
</div>