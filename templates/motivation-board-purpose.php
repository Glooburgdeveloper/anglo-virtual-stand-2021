
<div id="mainModal" class="anglo-modal purpose">
    <div class="modal-info">
        <div class="row">
            <div class="info">
                <span id="close-modal-btn" class="close-btn">Close</span>
                <h1>
                    Re-imagining mining to improve people’s lives.
                </h1>
                <div class="mobi-scroll">
                    <div class="scroll-content">
                        <p>
                            Mining has a smarter future – a future where you may barely notice some mines at all. And that’s a good thing.
                            Out of sight, but never out of mind, tomorrow’s ultimate mine will long provide the precious raw materials our
                            modern society needs. We are working to make this future a reality, to better connect the resources in the ground
                            to the people who need and value them.
                        </p>
                    </div>
                </div>
            </div>
            <div class="media-panel">
                <iframe width="100%" height="450" src="https://www.youtube.com/embed/VrGNoGDCs-0?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</div>