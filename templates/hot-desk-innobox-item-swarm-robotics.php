
<div class="modal-info">
    <div class="row">
        <div class="info">
            <span id="close-sec-modal-btn" class="close-btn">Close</span>
            <h1>
                Swarm robotics
            </h1>
            <div class="mobi-scroll">
                <div class="scroll-content">
                    <p>
                        Underground mining is a dangerous job. Since future orebodies are likely to be deeper, these hazards are
                        set to increase. Swarm robotic mining will deliver a new type of mine where humans will not be exposed to
                        the hazards currently faced by underground mining.
                    </p>
                </div>
            </div>
        </div>
        <div class="media-panel">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/8I11DzU6rx4?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>