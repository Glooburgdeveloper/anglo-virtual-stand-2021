var data = {
  "scenes": [
    {
      "id": "welcome-desk",
      "name": "Welcome desk",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.875104238381688,
          "pitch": -0.020422204979272607,
          "rotation": 0,
          "title": "Speaker Information",
          "target": "speaker-profiles",
          "icon": "speaker-profiles"
        },
        {
          "yaw": 1.0192889908413179,
          "pitch": -0.18722990350612712,
          "rotation": 0,
          "title": "Community Wall",
          "target": "community-wall",
          "icon": "community-wall"
        },
        {
          "yaw": 0.019079088186582993,
          "pitch": 0.0529855394452845,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        },
        // {
        //   "yaw": 0.5907558219815154,
        //   "pitch": -0.06114784579817112,
        //   "rotation": 0,
        //   "title": "Sustainable Mining Plan",
        //   "target": "4-right-wall-desk",
        //   "icon": "community-wall"
        // },
        {
          "yaw": -0.4736510047534761,
          "pitch": -0.11306110006375846,
          "rotation": 0,
          "title": "Our Purpose",
          "target": "6-left-wall",
          "icon": "motivation-board"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.04944189629145157,
          "pitch": 0.3375602292781785,
          "title": "Welcome desk",
          "modal": "welcome-desk",
          "icon": "welcome-desk"
        }
      ]
    },
    {
      "id": "speaker-profiles",
      "name": "Speaker Information",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.5342548966030183,
          "pitch": 0.15845238626671332,
          "rotation": 0,
          "title": "Safety first",
          "target": "5-left-wall-desk",
          "icon": "speaker-profiles"
        },
        {
          "yaw": 1.5948714198295102,
          "pitch": 0.09619041638491233,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        },
        {
          "yaw": -1.5010930176390644,
          "pitch": 0.15217618495536556,
          "rotation": 0,
          "title": "Welcome desk",
          "icon": "welcome-desk",
          "target": "welcome-desk"
        },
        {
          "yaw": 2.610417877805574,
          "pitch": 0.20911436784696846,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.014359449213895203,
          "pitch": -0.04190184196918523,
          "title": "Our speakers",
          "modal": "speaker-profiles-our-people",
          "icon": "speaker-profiles-our-people"
        },
        {
          "yaw": -2.9123536370938634,
          "pitch": -0.23073956123974781,
          "title": "Our Social Way",
          "modal": "community-wall-social-way",
          "icon": "community-wall-social-way"
        },
        {
          "yaw": 1.1110838439682986,
          "pitch": 0.27604379473995166,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.0242325471881486,
          "pitch": 0.2819645086582625,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        }
      ]
    },
    {
      "id": "community-wall",
      "name": "Community Wall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": -3.056992703732245,
        "pitch": 0.12515137464930604,
        "fov": 1.3768887047804423
      },
      "linkHotspots": [
        {
          "yaw": 2.6023015451770792,
          "pitch": 0.1785538740857433,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": -1.4788626620205463,
          "pitch": 0.16450067484973196,
          "rotation": 0,
          "title": "Welcome desk",
          "icon": "welcome-desk",
          "target": "welcome-desk"
        },
        {
          "yaw": 0.5380586854692488,
          "pitch": 0.1693541768261504,
          "rotation": 0,
          "title": "Safety first",
          "target": "5-left-wall-desk",
          "icon": "speaker-profiles"
        },
        {
          "yaw": 1.6015684216075412,
          "pitch": 0.10750854519921127,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.014359449213895203,
          "pitch": -0.04190184196918523,
          "title": "Our speakers",
          "modal": "speaker-profiles-our-people",
          "icon": "speaker-profiles-our-people"
        },
        {
          "yaw": -2.9123536370938634,
          "pitch": -0.23073956123974781,
          "title": "Our Social Way",
          "modal": "community-wall-social-way",
          "icon": "community-wall-social-way"
        },
        {
          "yaw": 1.1110838439682986,
          "pitch": 0.27604379473995166,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.0242325471881486,
          "pitch": 0.2819645086582625,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        }
      ]
    },
    {
      "id": "3-center-view",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.11907272916347,
          "pitch": 0.017651248861724866,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -1.8410664123626646,
          "pitch": 0.22123090903571807,
          "rotation": 0,
          "title": "Our Living Purpose Wall",
          "target": "6-left-wall",
          "icon": "motivation-board"
        },
        {
          "yaw": 1.8404594802470822,
          "pitch": 0.2270643886778103,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.6343490432790411,
          "pitch": 0.12388624913864277,
          "rotation": 0,
          "target": "discussion-wall",
          "title": "Thank you",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.575259778809162,
          "pitch": 0.03910693666623111,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": -0.1109796370763032,
          "pitch": -0.09618923412614677,
          "title": "Our global footprint",
          "modal": "worldwide-display",
          "icon": "worldwide-display"
        },
        {
          "yaw": -2.597069391733953,
          "pitch": 0.32628240828931965,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.6054611123214872,
          "pitch": 0.33572814616957736,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        },
        {
          "yaw": 0.9971205110105181,
          "pitch": 0.06846067145034951,
          "title": "Connect with us",
          "modal": "hot-desk-connect",
          "icon": "hot-desk-connect"
        },
        {
          "yaw": 0.06817686349662289,
          "pitch": 0.2831847541379666,
          "title": "Brochure downloads",
          "modal": "hot-desk-brochures",
          "icon": "hot-desk-brochures"
        },
        {
          "yaw": -1.1983399299360933,
          "pitch": 0.10161276397537122,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "4-right-wall-desk",
      "name": "Community Wall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": -0.4766091418907692,
        "pitch": -0.09848001657783811,
        "fov": 1.3768887047804423
      },
      "linkHotspots": [
        {
          "yaw": 1.5286015880943093,
          "pitch": 0.05602904598549685,
          "rotation": 0,
          "title": "Thank you",
          "target": "discussion-wall",
          "icon": "discussion-wall"
        },
        {
          "yaw": 0.25024514075098736,
          "pitch": 0.09804742290870294,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        },
        {
          "yaw": -0.3707157725806507,
          "pitch": 0.09227926905547434,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.6485964205295751,
          "pitch": -0.3030653943941779,
          "title": "Sustainable Mining Plan",
          "icon": "community-wall-sustainable-mining",
          "modal": "community-wall-sustainable-mining"
        },
        {
          "yaw": 0.9843055119179631,
          "pitch": 0.16665025677540157,
          "title": "Connect with us",
          "icon": "hot-desk-connect",
          "modal": "hot-desk-connect"
        }
      ]
    },
    {
      "id": "5-left-wall-desk",
      "name": "Speaker information",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": 0.6296610916810366,
        "pitch": -0.0038226918046859737,
        "fov": 1.2748677168646196
      },
      "linkHotspots": [
        {
          "yaw": 0.2801835108958528,
          "pitch": 0.10763130220285255,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -0.209302232473199,
          "pitch": 0.11707700975202151,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        },
        {
          "yaw": -1.4994054225451983,
          "pitch": 0.05286034317586186,
          "rotation": 0,
          "title": "Thank you",
          "target": "discussion-wall",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.6467816655287759,
          "pitch": 0.3979797937427971,
          "title": "Our WeCare Programme",
          "modal": "motivation-board-care",
          "icon": "motivation-board-care"
        },
        {
          "yaw": 0.812083876767872,
          "pitch": 0.23753805433418407,
          "title": "Safety first",
          "modal": "speaker-profiles-mine-safety",
          "icon": "speaker-profiles-mine-safety"
        },
        {
          "yaw": -1.1349427458584866,
          "pitch": 0.04501315190477406,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": -0.8216992420489433,
          "pitch": 0.13745934293663353,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "6-left-wall",
      "name": "Our Living Purpose Wall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": -0.6820209692408721,
        "pitch": -0.020186098726274082,
        "fov": 1.4185322104917357
      },
      "linkHotspots": [
        {
          "yaw": -1.4532606981421132,
          "pitch": 0.03673032007077914,
          "rotation": 0,
          "title": "Thank you",
          "target": "discussion-wall",
          "icon": "discussion-wall"
        },
        {
          "yaw": 0.2836057921349031,
          "pitch": 0.11064383753776497,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -0.20817087304367732,
          "pitch": 0.1254221443379535,
          "rotation": 0,
          "title": "Innovation hub",
          "target": "3-center-view",
          "icon": "hot-desk"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.6467816655287759,
          "pitch": 0.3979797937427971,
          "title": "Our WeCare Programme",
          "modal": "motivation-board-care",
          "icon": "motivation-board-care"
        },
        {
          "yaw": 0.812083876767872,
          "pitch": 0.23753805433418407,
          "title": "Safety first",
          "modal": "speaker-profiles-mine-safety",
          "icon": "speaker-profiles-mine-safety"
        },
        {
          "yaw": -1.1349427458584866,
          "pitch": 0.04501315190477406,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": -0.8216992420489433,
          "pitch": 0.13745934293663353,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "discussion-wall",
      "name": "Thank You",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": 0,
        "pitch": 0,
        "fov": 1.3768887047804423
      },
      "linkHotspots": [
        {
          "yaw": -0.5145444775193688,
          "pitch": 0.2579818236942124,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.7752055405985416,
          "pitch": 0.14919192558200578,
          "rotation": 0,
          "title": "Our Purpose",
          "target": "6-left-wall",
          "icon": "motivation-board"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "innobox-panels-1",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": 2.596790553207761,
        "pitch": 0.09207794471360309,
        "fov": 1.3768887047804423
      },
      "linkHotspots": [
        {
          "yaw": -3.11907272916347,
          "pitch": 0.017651248861724866,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -1.8410664123626646,
          "pitch": 0.22123090903571807,
          "rotation": 0,
          "title": "Our Living Purpose Wall",
          "target": "6-left-wall",
          "icon": "motivation-board"
        },
        {
          "yaw": 1.8404594802470822,
          "pitch": 0.2270643886778103,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.6343490432790411,
          "pitch": 0.12388624913864277,
          "rotation": 0,
          "target": "discussion-wall",
          "title": "Thank you",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.575259778809162,
          "pitch": 0.03910693666623111,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": 0.5152335347326336,
          "pitch": -0.20017441946989756,
          "title": "Our global footprint",
          "modal": "worldwide-display",
          "icon": "worldwide-display"
        },
        {
          "yaw": -2.597069391733953,
          "pitch": 0.32628240828931965,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.6054611123214872,
          "pitch": 0.33572814616957736,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        },
        {
          "yaw": 0.9971205110105181,
          "pitch": 0.06846067145034951,
          "title": "Connect with us",
          "modal": "hot-desk-connect",
          "icon": "hot-desk-connect"
        },
        {
          "yaw": 0.06817686349662289,
          "pitch": 0.2831847541379666,
          "title": "Brochure downloads",
          "modal": "hot-desk-brochures",
          "icon": "hot-desk-brochures"
        },
        {
          "yaw": -1.1983399299360933,
          "pitch": 0.10161276397537122,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "innobox-panels-2",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": -2.597069391733953,
        "pitch": 0.09207794471360309,
        "fov": 1.3768887047804423
      },
      "linkHotspots": [
        {
          "yaw": -3.11907272916347,
          "pitch": 0.017651248861724866,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -1.8410664123626646,
          "pitch": 0.22123090903571807,
          "rotation": 0,
          "title": "Our Living Purpose Wall",
          "target": "6-left-wall",
          "icon": "motivation-board"
        },
        {
          "yaw": 1.8404594802470822,
          "pitch": 0.2270643886778103,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.6343490432790411,
          "pitch": 0.12388624913864277,
          "rotation": 0,
          "target": "discussion-wall",
          "title": "Thank you",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.575259778809162,
          "pitch": 0.03910693666623111,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": 0.5152335347326336,
          "pitch": -0.20017441946989756,
          "title": "Our global footprint",
          "modal": "worldwide-display",
          "icon": "worldwide-display"
        },
        {
          "yaw": -2.597069391733953,
          "pitch": 0.32628240828931965,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.6054611123214872,
          "pitch": 0.33572814616957736,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        },
        {
          "yaw": 0.9971205110105181,
          "pitch": 0.06846067145034951,
          "title": "Connect with us",
          "modal": "hot-desk-connect",
          "icon": "hot-desk-connect"
        },
        {
          "yaw": 0.06817686349662289,
          "pitch": 0.2831847541379666,
          "title": "Brochure downloads",
          "modal": "hot-desk-brochures",
          "icon": "hot-desk-brochures"
        },
        {
          "yaw": -1.1983399299360933,
          "pitch": 0.10161276397537122,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "3-center-view-connect",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": 0.9971205110105181,
        "pitch": 0.06846067145034951,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.11907272916347,
          "pitch": 0.017651248861724866,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -1.8410664123626646,
          "pitch": 0.22123090903571807,
          "rotation": 0,
          "title": "Our Living Purpose Wall",
          "target": "6-left-wall",
          "icon": "motivation-board"
        },
        {
          "yaw": 1.8404594802470822,
          "pitch": 0.2270643886778103,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.6343490432790411,
          "pitch": 0.12388624913864277,
          "rotation": 0,
          "target": "discussion-wall",
          "title": "Thank you",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.575259778809162,
          "pitch": 0.03910693666623111,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": -0.1109796370763032,
          "pitch": -0.09618923412614677,
          "title": "Our global footprint",
          "modal": "worldwide-display",
          "icon": "worldwide-display"
        },
        {
          "yaw": -2.597069391733953,
          "pitch": 0.32628240828931965,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.6054611123214872,
          "pitch": 0.33572814616957736,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        },
        {
          "yaw": 0.9971205110105181,
          "pitch": 0.06846067145034951,
          "title": "Connect with us",
          "modal": "hot-desk-connect",
          "icon": "hot-desk-connect"
        },
        {
          "yaw": 0.06817686349662289,
          "pitch": 0.2831847541379666,
          "title": "Brochure downloads",
          "modal": "hot-desk-brochures",
          "icon": "hot-desk-brochures"
        },
        {
          "yaw": -1.1983399299360933,
          "pitch": 0.10161276397537122,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    },
    {
      "id": "3-center-view-speak",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "yaw": -0.575259778809162,
        "pitch": 0.03910693666623111,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.11907272916347,
          "pitch": 0.017651248861724866,
          "rotation": 0,
          "target": "welcome-desk",
          "title": "Welcome desk",
          "icon": "welcome-desk"
        },
        {
          "yaw": -1.8410664123626646,
          "pitch": 0.22123090903571807,
          "rotation": 0,
          "title": "Our Living Purpose Wall",
          "target": "6-left-wall",
          "icon": "motivation-board"
        },
        {
          "yaw": 1.8404594802470822,
          "pitch": 0.2270643886778103,
          "rotation": 0,
          "title": "Sustainable Mining Plan",
          "target": "4-right-wall-desk",
          "icon": "community-wall"
        },
        {
          "yaw": 0.6343490432790411,
          "pitch": 0.12388624913864277,
          "rotation": 0,
          "target": "discussion-wall",
          "title": "Thank you",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.575259778809162,
          "pitch": 0.03910693666623111,
          "title": "Come chat to us",
          "modal": "speak-to-us",
          "icon": "speak-to-us"
        },
        {
          "yaw": -0.1109796370763032,
          "pitch": -0.09618923412614677,
          "title": "Our global footprint",
          "modal": "worldwide-display",
          "icon": "worldwide-display"
        },
        {
          "yaw": -2.597069391733953,
          "pitch": 0.32628240828931965,
          "title": "Step-change innovations ",
          "modal": "hot-desk-innobox",
          "icon": "hot-desk-innobox"
        },
        {
          "yaw": 2.6054611123214872,
          "pitch": 0.33572814616957736,
          "title": "Futuresmart Mining<sup>TM</sup>",
          "modal": "hot-desk-future-smart",
          "icon": "hot-desk-future-smart"
        },
        {
          "yaw": 0.9971205110105181,
          "pitch": 0.06846067145034951,
          "title": "Connect with us",
          "modal": "hot-desk-connect",
          "icon": "hot-desk-connect"
        },
        {
          "yaw": 0.06817686349662289,
          "pitch": 0.2831847541379666,
          "title": "Brochure downloads",
          "modal": "hot-desk-brochures",
          "icon": "hot-desk-brochures"
        },
        {
          "yaw": -1.1983399299360933,
          "pitch": 0.10161276397537122,
          "title": "Our Purpose",
          "modal": "motivation-board-purpose",
          "icon": "motivation-board-purpose"
        }
      ]
    }
  ],
  "name": "Anglo American Virtual Stand",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
