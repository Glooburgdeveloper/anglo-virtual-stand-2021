'use strict';
var isMobile = navigator.userAgent.match(/Android|webOS|iPhone|iPod|BlackBerry|BB10|IEMobile|Opera Mini/i);
var isiPad = navigator.userAgent.match(/iPad/i);
var scrWidth = screen.width;

document.addEventListener('DOMContentLoaded', () => {
  window.addEventListener("touchstart", function(e) {passive: true} );
  function setDefaults() {

    if (isMobile != null && scrWidth <= 680 || scrWidth < 680) {
      $(window).on("orientationchange", function (event) {
        if (window.orientation == 90 || window.orientation == -90) {
          $('#rotateScreen').addClass('active');
        } else {
          $('#rotateScreen').removeClass('active');
        }
      });
    }
    else if (isMobile != null || isiPad != null && scrWidth < 800 || isMobile != null && scrWidth == 1280 || scrWidth < 1280) {
      $(window).on("orientationchange", function (event) {
        if (window.orientation == 90 || window.orientation == -90) {
          location.reload();
        } else {
          location.reload();
        }
      });

    }
    else {

    }
  }
  setTimeout(function(){
    $('#page-loader').addClass('grow');
    setTimeout(function(){
      $('#page-loader').addClass('hidden');

      setTimeout(function(){
        $('#page-loader').remove();
        setDefaults();
      }, 900);
    }, 800);
  }, 1000);

});
(function() {
  var Marzipano = window.Marzipano;
  var screenfull = window.screenfull;
  var data = window.data;
  var modal = '';
  var view;

  // var scrWidth = screen.width;


  // Grab elements from DOM.
  var panoElement = document.querySelector('#pano');
  var sceneNameElement = document.querySelector('#titleBar .sceneName');
  var sceneListElement = document.querySelector('#sceneList');
  var sceneElements = document.querySelectorAll('#sceneList .scene');
  var sceneListToggleElement = document.querySelector('#sceneListToggle');
  var autorotateToggleElement = document.querySelector('#autorotateToggle');
  var fullscreenToggleElement = document.querySelector('#fullscreenToggle');

  // Detect desktop or mobile mode.
  if (window.matchMedia) {
    var setMode = function() {
      if (mql.matches) {
        document.body.classList.remove('desktop');
        document.body.classList.add('mobile');
      } else {
        document.body.classList.remove('mobile');
        document.body.classList.add('desktop');
      }
    };
    var mql = matchMedia("(max-width: 500px), (max-height: 500px)");
    setMode();
    mql.addListener(setMode);
  } else {
    document.body.classList.add('desktop');
  }

  // Detect whether we are on a touch device.
  document.body.classList.add('no-touch');
  window.addEventListener('touchstart', function() {
    document.body.classList.remove('no-touch');
    document.body.classList.add('touch');
  });

  // Viewer options.
  var viewerOpts = {
    controls: {
      mouseViewMode: data.settings.mouseViewMode
    }
  };

  // Initialize viewer.
  var viewer = new Marzipano.Viewer(panoElement, viewerOpts);
  // Register the custom control method.
  var deviceOrientationControlMethod = new DeviceOrientationControlMethod();
  var controls = viewer.controls();
  controls.registerMethod('deviceOrientation', deviceOrientationControlMethod);


  // Create scenes.
  var scenes = data.scenes.map(function(data) {
    var urlPrefix = "scenes";

    var source = Marzipano.ImageUrlSource.fromString(
        urlPrefix + "/" + data.id + "/{z}/{f}/{y}/{x}.jpg",
        { cubeMapPreviewUrl: urlPrefix + "/" + data.id + "/preview.jpg" });
    var geometry = new Marzipano.CubeGeometry(data.levels);

    var limiter = Marzipano.RectilinearView.limit.traditional(data.faceSize, 100*Math.PI/180, 120*Math.PI/180);
    view = new Marzipano.RectilinearView(data.initialViewParameters, limiter);

    var scene = viewer.createScene({
      source: source,
      geometry: geometry,
      view: view,
      pinFirstLevel: true
    });

    // Create link hotspots.
    data.linkHotspots.forEach(function(hotspot) {
      var element = createLinkHotspotElement(hotspot);
      scene.hotspotContainer().createHotspot(element, { yaw: hotspot.yaw, pitch: hotspot.pitch });
    });

    // Create info hotspots.
    data.infoHotspots.forEach(function(hotspot) {
      var element = createInfoHotspotElement(hotspot);
      scene.hotspotContainer().createHotspot(element, { yaw: hotspot.yaw, pitch: hotspot.pitch });
    });

    return {
      data: data,
      scene: scene,
      view: view
    };
  });

  // Set up control for enabling/disabling device orientation.

  var enabled = false;

  var toggleElement = document.getElementById('toggleDeviceOrientation');

  function requestPermissionForIOS() {
    window.DeviceOrientationEvent.requestPermission()
        .then(response => {
          if (response === 'granted') {
            enableDeviceOrientation();
          }
        }).catch((e) => {
      //console.error(e);
    });
  }

  function enableDeviceOrientation() {
    deviceOrientationControlMethod.getPitch(function (err, pitch) {
      if (!err) {
        view.setPitch(pitch);
      }
    });
    controls.enableMethod('deviceOrientation');
    enabled = true;
    toggleElement.classList.add('enabled');
  }

  function enableOrientation() {
    if (window.DeviceOrientationEvent) {
      if (typeof (window.DeviceOrientationEvent.requestPermission) == 'function') {
        requestPermissionForIOS();
      } else {
        enableDeviceOrientation();
      }
    }
  }

  function disableOrientation() {
    controls.disableMethod('deviceOrientation');
    enabled = false;
    toggleElement.classList.remove('enabled');
  }

  function toggleOrientation() {
    if (enabled) {
      disableOrientation();
    } else {
      enableOrientation();
    }
  }

  toggleElement.addEventListener('click', toggleOrientation);

  // Set up autorotate, if enabled.
  var autorotate = Marzipano.autorotate({
    yawSpeed: 0.007,
    targetPitch: 0,
    targetFov: Math.PI/2
  });
  if (data.settings.autorotateEnabled) {
    autorotateToggleElement.classList.add('enabled');
  }

  // Set handler for autorotate toggle.
  autorotateToggleElement.addEventListener('click', toggleAutorotate);

  // Set up fullscreen mode, if supported.
  if (screenfull.enabled && data.settings.fullscreenButton) {
    document.body.classList.add('fullscreen-enabled');
    fullscreenToggleElement.addEventListener('click', function() {
      screenfull.toggle();
    });
    screenfull.on('change', function() {
      if (screenfull.isFullscreen) {
        fullscreenToggleElement.classList.add('enabled');
      } else {
        fullscreenToggleElement.classList.remove('enabled');
      }
    });
  } else {
    document.body.classList.add('fullscreen-disabled');
  }

  // Set handler for scene list toggle.
  sceneListToggleElement.addEventListener('click', toggleSceneList);

  // Start with the scene list open on desktop.
  if (!document.body.classList.contains('mobile')) {
    hideSceneList();
  }

  // Set handler for scene switch.
  // scenes.forEach(function(scene) {
  //   var el = document.querySelector('#sceneList .scene[data-id="' + scene.data.id + '"]');
  //   el.addEventListener('click', function() {
  //     //console.log(el);
  //     //switchScene(scene);
  //     // On mobile, hide scene list after selecting a scene.
  //     hideSceneList();
  //   });
  // });
  $('#sceneList .scene').off().on('click', function(){
    var sceneSet = $(this).attr('data-id');
    switch (sceneSet) {
      case "welcome-desk":
        switchScene(scenes[0]);
        break;
      case "speaker-profiles":
        switchScene(scenes[1]);
        break;
      case "community-wall":
        switchScene(scenes[2]);
        break;
      case "3-center-view":
        switchScene(scenes[3]);
        break;
      case "4-right-wall-desk":
        switchScene(scenes[4]);
        break;
      case "5-left-wall-desk":
        switchScene(scenes[5]);
        break;
      case "6-left-wall":
        switchScene(scenes[6]);
        break;
      case "discussion-wall":
        switchScene(scenes[7]);
        break;
      case "innobox-panels-1":
        switchScene(scenes[8]);
        break;
      case "innobox-panels-2":
        switchScene(scenes[9]);
        break;
      case "3-center-view-connect":
        switchScene(scenes[10]);
        break;
      case "3-center-view-speak":
        switchScene(scenes[11]);
        break;
    }
    //switchScene(sceneSet);
    hideSceneList();
  });

  // DOM elements for view controls.
  var viewUpElement = document.querySelector('#viewUp');
  var viewDownElement = document.querySelector('#viewDown');
  var viewLeftElement = document.querySelector('#viewLeft');
  var viewRightElement = document.querySelector('#viewRight');
  var viewInElement = document.querySelector('#viewIn');
  var viewOutElement = document.querySelector('#viewOut');

  // Dynamic parameters for controls.
  var velocity = 0.7;
  var friction = 3;

  // Associate view controls with elements.
  //var controls = viewer.controls();
  controls.registerMethod('upElement',    new Marzipano.ElementPressControlMethod(viewUpElement,     'y', -velocity, friction), true);
  controls.registerMethod('downElement',  new Marzipano.ElementPressControlMethod(viewDownElement,   'y',  velocity, friction), true);
  controls.registerMethod('leftElement',  new Marzipano.ElementPressControlMethod(viewLeftElement,   'x', -velocity, friction), true);
  controls.registerMethod('rightElement', new Marzipano.ElementPressControlMethod(viewRightElement,  'x',  velocity, friction), true);
  controls.registerMethod('inElement',    new Marzipano.ElementPressControlMethod(viewInElement,  'zoom', -velocity, friction), true);
  controls.registerMethod('outElement',   new Marzipano.ElementPressControlMethod(viewOutElement, 'zoom',  velocity, friction), true);

  function sanitize(s) {
    return s.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
  }

  function switchScene(scene) {
    stopAutorotate();

    scene.view.setParameters(scene.data.initialViewParameters);
    scene.scene.switchTo();

    if (scrWidth > 769 ) {
      startAutorotate();
    }

    updateSceneName(scene);
    updateSceneList(scene);
  }

  function updateSceneName(scene) {
    sceneNameElement.innerHTML = sanitize(scene.data.name);
  }

  function updateSceneList(scene) {
    for (var i = 0; i < sceneElements.length; i++) {
      var el = sceneElements[i];
      if (el.getAttribute('data-id') === scene.data.id) {
        el.classList.add('current');
      } else {
        el.classList.remove('current');
      }
    }
  }

  function hideSceneList() {
    sceneListElement.classList.remove('enabled');
    sceneListToggleElement.classList.remove('enabled');
  }

  function toggleSceneList() {
    sceneListElement.classList.toggle('enabled');
    sceneListToggleElement.classList.toggle('enabled');
  }

  function startAutorotate() {
    if (!autorotateToggleElement.classList.contains('enabled')) {
      return;
    }
    viewer.startMovement(autorotate);
    viewer.setIdleMovement(3000, autorotate);
  }

  function stopAutorotate() {
    viewer.stopMovement();
    viewer.setIdleMovement(Infinity);
  }

  // Autorotate only on desktop.
  function toggleAutorotate() {
    if (autorotateToggleElement.classList.contains('enabled')) {
      autorotateToggleElement.classList.remove('enabled');
      stopAutorotate();
    } else {
      autorotateToggleElement.classList.add('enabled');
      startAutorotate();
    }
  }

  function createLinkHotspotElement(hotspot) {

    // Create wrapper element to hold icon and tooltip.
    var wrapper = document.createElement('div');
    wrapper.classList.add('hotspot');
    wrapper.classList.add('link-hotspot');
    wrapper.classList.add(hotspot.icon);
    wrapper.setAttribute('data-modal', hotspot.modal);

    var header = document.createElement('h1');
    header.innerHTML = hotspot.title;
    wrapper.appendChild(header);

    // Add click event handler.
    wrapper.addEventListener('click', function() {
      switchScene(findSceneById(hotspot.target));
    });

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    stopTouchAndScrollEventPropagation(wrapper);


    return wrapper;
  }

  function createInfoHotspotElement(hotspot) {

    // Create wrapper element to hold icon and tooltip.
    var wrapper = document.createElement('div');
    wrapper.classList.add('hotspot');
    wrapper.classList.add('info-hotspot');
    wrapper.classList.add(hotspot.icon);
    wrapper.setAttribute('data-modal', hotspot.modal);

    var header = document.createElement('h1');
    header.innerHTML = hotspot.title;
    wrapper.appendChild(header);

    var toggle = function() {
      wrapper.classList.toggle('visible');
    };

    var modalFetch = function(){
      modal = wrapper.getAttribute('data-modal');
      modalSet(modal);
    };

    // Show content when hotspot is clicked.
    wrapper.addEventListener('click', modalFetch);

    // Hide content when close icon is clicked.
    //modal.querySelector('.info-hotspot-close-wrapper').addEventListener('click', toggle);

    // Prevent touch and scroll events from reaching the parent element.
    // This prevents the view control logic from interfering with the hotspot.
    stopTouchAndScrollEventPropagation(wrapper);

    return wrapper;
  }

  function modalSet(id) {

    $.get("templates/" + id + ".php", function (data) {
      $("#modalHolder").append(data);
      setTimeout(function(){
        var element = document.getElementById('mainModal');
        element.classList.add('active');

        // incase need to load functional after
        switch (id) {
          case "hot-desk-innobox":
            var overlay = document.getElementById('mainOverlay');
            overlay.classList.add('active');
            break;
          case "welcome-desk":
            var overlay = document.getElementById('mainOverlay');
            overlay.classList.add('active');
            setTimeout(function(){
              $('#eploreStart').off().on('click', function(){
                modalRemove();
                switchScene(scenes[1]);
              });
              $('#chatBtn').off().on('click', function(){
                modalRemove();
                switchScene(scenes[11]);
                setTimeout(function(){
                  modalSet('speak-to-us');
                }, 1000);

              });
            }, 1000);
            break;
          case "hot-desk-connect":
            $('#chatBtn').off().on('click', function(){
              modalRemove();
              switchScene(scenes[11]);
              setTimeout(function(){
                modalSet('speak-to-us');
              }, 1000);

            });
            break;
          case "speak-to-us":

            break;
        }

      }, 200);

      var closeBtn = document.getElementById('close-modal-btn');
      closeBtn.addEventListener('click', modalRemove);
    });
  }

  function modalRemove() {
    var mainElement = document.getElementById('modalHolder');
    var element = document.getElementById('mainModal');
    var overlay = document.getElementById('mainOverlay');
    if (overlay != null){
      overlay.classList.remove('active');
    }
    element.classList.remove('active');


    setTimeout(function(){
      mainElement.innerHTML = '';
    }, 800);

  }

  // Prevent touch and scroll events from reaching the parent element.
  function stopTouchAndScrollEventPropagation(element, eventList) {
    var eventList = [ 'touchstart', 'touchmove', 'touchend', 'touchcancel', 'wheel', 'mousewheel' ];
    for (var i = 0; i < eventList.length; i++) {
      element.addEventListener(eventList[i], function(event) {
        event.stopPropagation();
      });
    }
  }

  function findSceneById(id) {
    for (var i = 0; i < scenes.length; i++) {
      if (scenes[i].data.id === id) {
        return scenes[i];
      }
    }
    return null;
  }

  function findSceneDataById(id) {
    for (var i = 0; i < data.scenes.length; i++) {
      if (data.scenes[i].id === id) {
        return data.scenes[i];
      }
    }
    return null;
  }

  function explore(){
    switchScene(scenes[4]);
  }

  // Display the initial scene.
  switchScene(scenes[0]);

  setTimeout(function(){
    modalSet('welcome-desk');
  }, 1000);

  if (scrWidth < 769 ) {
    stopAutorotate();
  }


})();
