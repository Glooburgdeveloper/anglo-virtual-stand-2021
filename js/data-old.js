/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var data = {
  "scenes": [
      {
      "id": "welcome-desk",
      "name": "Welcome Desk",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "welcome-desk",
          "title": "Welcome Desk",
          "icon": "welcome-desk"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "welcome-desk",
          "title": "Welcome Desk",
          "modal": "welcome-desk"
        }
      ]
    },
      {
      "id": "speaker-profiles",
      "name": "Speaker Information",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "speaker-profiles",
          "title": "Speaker Information",
          "icon": "speaker-profiles",
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.4606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "speaker-profiles-mine-safety",
          "title": "Mine Safety",
          "modal": "speaker-profiles-mine-safety"
        },
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "speaker-profiles-our-people",
          "title": "Our People",
          "modal": "speaker-profiles-our-people"
        }
      ]
    },
      {
      "id": "community-wall",
      "name": "Community Wall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "community-wall",
          "title": "Community Wall",
          "icon": "community-wall",
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "community-wall-social-way",
          "title": "Social Way",
          "modal": "community-wall-social-way"
        },
        {
          "yaw": -0.4606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "community-wall-sustainable-mining",
          "title": "Sustainable Mining Plan",
          "modal": "community-wall-sustainable-mining"
        }
      ]
    },
      {
      "id": "motivation-board",
      "name": "Our Living Purpose Wall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "motivation-board",
          "title": "Our Living Purpose Wall",
          "icon": "motivation-board",
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.4606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "motivation-board-purpose",
          "title": "Our Purpose",
          "modal": "motivation-board-purpose"
        },
        {
          "yaw": -0.1306464893205768,
          "pitch": -0.12433292221669205,
          "icon": "motivation-board-care",
          "title": "We Care",
          "modal": "motivation-board-care"
        }
      ]
    },
      {
      "id": "hot-desk",
      "name": "Innovation Hub",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 4096,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "hot-desk",
          "title": "Innovation Hub",
          "icon": "hot-desk",
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "hot-desk-future-smart",
          "title": "Futuresmart",
          "modal": "hot-desk-future-smart"
        },
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.57433292221669205,
          "icon": "hot-desk-innobox",
          "title": "Innobox",
          "modal": "hot-desk-innobox"
        },
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.77433292221669205,
          "icon": "hot-desk-brochures",
          "title": "Brochures",
          "modal": "hot-desk-brochures"
        },
        {
          "yaw": -0.2606464893205768,
          "pitch": -0.37433292221669205,
          "icon": "hot-desk-connect",
          "title": "Connect",
          "modal": "hot-desk-connect"
        }
      ]
    },
      {
      "id": "worldwide-display",
      "name": "Where We Operate",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "worldwide-display",
          "title": "Where We Operate",
          "icon": "worldwide-display"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "worldwide-display",
          "title": "Worldwide Display",
          "modal": "worldwide-display"
        },
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.27433292221669205,
          "icon": "speak-to-us",
          "title": "Speak to us",
          "modal": "speak-to-us"
        }
      ]
    },
      {
      "id": "discussion-wall",
      "name": "Thank You",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 750,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3152585099587224,
          "pitch": 0.045251205931975846,
          "rotation": 5.497787143782138,
          "target": "discussion-wall",
          "title": "Thank You",
          "icon": "discussion-wall"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1606464893205768,
          "pitch": -0.17433292221669205,
          "icon": "discussion-wall-speech",
          "title": "Thank you Note",
          "modal": "discussion-wall-speech"
        }
      ]
    },
  ],
  "name": "Anglo American Virtual Stand",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
