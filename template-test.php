<?php header("Access-Control-Allow-Origin: *");?>

<!DOCTYPE html>
<html>
<head>
    <title>Template | Anglo American South Africa</title>
    <meta name="description" content="" />
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />
    <style> @-ms-viewport { width: device-width; } </style>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>

        <?php include 'templates/speaker-profiles-our-people.php'; ?>

        <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    </body>
</html>
